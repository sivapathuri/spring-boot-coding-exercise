# See
# https://github.com/intuit/karate#syntax-guide
# for how to write feature scenarios
Feature: As an api user I want to retrieve hottest repositories in github in last week

  Scenario: Get top 100 [default count] hottest repositories in last week
    Given url microserviceUrl
    And path '/trending'
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    And match response == '#[100]'

  Scenario: Get top 20 hottest repositories in last week
    Given url microserviceUrl
    And path '/trending'
    And param count = 20
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    And match response == '#[20]'

  Scenario: Get top 150 hottest repositories (in descending order) in last week
    * def Collections = Java.type('java.util.Collections')

    Given url microserviceUrl
    And path '/trending'
    And param count = 150
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    And match response == '#[150]'
    * def before = $response[*].watchers_count
    * copy after = before
    * Collections.sort(after, Collections.reverseOrder())
    * match before == after
