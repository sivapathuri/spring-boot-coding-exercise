package com.telstra.codechallenge.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RepositoryController {

    @Autowired
    private RepositoryService repositoryService;

    @RequestMapping(path = "/trending", method = RequestMethod.GET)
    public List<Repository> getTrendingRepositores(@RequestParam(value = "count", defaultValue = "100") int count) {
        return repositoryService.getTrendingRepositories(count);
    }
}
