package com.telstra.codechallenge.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@Service
public class RepositoryService {
    @Value("${api.github.search.url}")
    private String githubBaseUrl;

    @Value("${api.github.search.trending.fromLastDays}")
    private int trendingDaysFrom;

    @Value("${api.github.search.max_results_per_page}")
    private int maxResultsPerPage;

    @Autowired
    private RestTemplate restTemplate;

    public List<Repository> getTrendingRepositories(int count) {
        LocalDate trendingStartDate = getTrendingStartDate();
        RepositoryResponse resultsPerPage = getResultsPerPage(1, trendingStartDate);
        int total_count = resultsPerPage.getTotal_count();
        List<Repository> repositories = resultsPerPage.getItems();
        for(int i = maxResultsPerPage; i < Math.min(count, total_count); i += maxResultsPerPage) {
            resultsPerPage = getResultsPerPage(i / maxResultsPerPage + 1, trendingStartDate);
            repositories.addAll(resultsPerPage.getItems());
        }
        return repositories.subList(0, count);
    }

    private LocalDate getTrendingStartDate() {
        LocalDate currentDate =LocalDate.now();
        return currentDate.minusDays(trendingDaysFrom);
    }

    private RepositoryResponse getResultsPerPage(int currentPage, LocalDate trendingStartDate) {
        String encodeUrl = UriComponentsBuilder.fromHttpUrl(githubBaseUrl)
                .queryParam("q", "created:>" + trendingStartDate)
                .queryParam("sort", "stars")
                .queryParam("order", "desc")
                .queryParam("per_page", maxResultsPerPage)
                .queryParam("page", currentPage)
                .build(false).toString();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(new MediaType[] { new MediaType("application","vnd.github.v3+json")}));
        HttpEntity<RepositoryResponse> entity = new HttpEntity<>(headers);

        ResponseEntity<RepositoryResponse> response = restTemplate.exchange(
                encodeUrl,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<RepositoryResponse>(){});
        RepositoryResponse repositoriesResponse = response.getBody();
        return repositoriesResponse;
    }
}
